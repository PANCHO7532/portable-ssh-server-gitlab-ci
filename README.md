# portable-ssh-server-gitlab-ci
Obtain GitLab CI/CD VMs full SSH shell access for diverse purposes (also configured for VPN over SSH services)

![xd](screenshot.png)

# Usage
1) Create an account on [ngrok](https://dashboard.ngrok.com/signup), and copy your auth token displayed [here](https://dashboard.ngrok.com/auth)
2) Fork this repository
3) Go to Settings > CI/CD, expand "Runners" and be sure that the option "Enable shared runners for this project" is checked
4) On the same page, expand "Artifacts", uncheck "Keep artifacts from most recent successful jobs"
5) On the same page, expand "Variables", and click on "Add variable", create the following variables:
```
Key: NGROK_AUTH_TOKEN
Value: (obviously you insert the ngrok auth token of your account that you obtained in step 1)
Flags: Protect, Mask

Key: RUNNER_PASSWORD
Value: (The password that you will be using at the moment of connection)
Flags: Protect, Mask
```
All of those variables are required for the script to work, if you forget to add one, then it will throw an error

6) Trigger an build, by editing this README or uploading anything to your repository, don't modify the contents of the resources or scripts folders
7) Go to CI/CD > Jobs, open the running job (might be labeled in blue), and wait until the last step where it will hang forever
8) Visit ngrok's tunnel list [dashboard](https://dashboard.ngrok.com/status/tunnels)
9) Take note of the active tunnel host and port
10) Connect to the host and port combination with an SSH client of your preference
11) As username, write "root" and as password, the password that you wrote in the RUNNER_PASSWORD secret in step 5
12) Once connected, you will get a fully working terminal!

## Duration of the runners
You can see how long the runner will last on CI/CD > Jobs, click on your active job and near the log you will see a column with the details of the job, the value under Timeout defines how long it will last (generally for one hour).

## Some issues
- I/O operations might throw an error in rare cases
- APT Package Manager has a slightly reduced list on what you might install or not, if you can't install something, search the .deb and install it manually